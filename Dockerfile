FROM php:7.3-apache

RUN apt update -y

RUN apt install libpng-dev libmcrypt-dev libjpeg62-turbo-dev libfreetype6-dev imagemagick \
libmagickwand-dev zip libzip-dev curl libcurl4-gnutls-dev -y

RUN set -x && printf "\n" | pecl -q install mcrypt && \
printf "\n" | pecl -q install imagick && \
docker-php-ext-enable mcrypt imagick && \
docker-php-ext-install -j$(nproc) mysqli gd curl mbstring zip && \
docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
sed -i -e 's/^max_execution_time\s*=.*/max_execution_time = 120/' $PHP_INI_DIR/php.ini && \
sed -i -e 's/^post_max_size\s*=.*/post_max_size = 16M/' $PHP_INI_DIR/php.ini && \
sed -i -e 's/^upload_max_filesize\s*=.*/upload_max_filesize = 16M/' $PHP_INI_DIR/php.ini

COPY src/ /var/www/html/

RUN sed -i -e 's/$CFG->W_HOST\s*=.*/$CFG->W_HOST = \"db\";/' /var/www/html/Install/index.php && \
sed -i -e 's/$CFG->R_HOST\s*=.*/$CFG->R_HOST = \"db\";/' /var/www/html/Install/index.php && \
sed -i -e 's/$CFG->R_USER\s*=.*/$CFG->R_USER = \"ianseo\";/' /var/www/html/Install/index.php && \
sed -i -e 's/$CFG->R_PASS\s*=.*/$CFG->R_PASS = \"ianseo\";/' /var/www/html/Install/index.php

RUN rm -f /var/www/html/Common/config.inc.php && \
chown -R www-data:www-data /var/www/html/

VOLUME /var/www/html/Modules/Custom

EXPOSE 80
